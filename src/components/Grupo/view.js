import React from 'react';
import PropTypes from 'prop-types';

const Grupo = ({ grupo }) => {
  if (Object.keys(grupo).length === 0) return null;
  const {
    strArtistThumb,
    strGenre,
    strArtist,
    strBiographyES,
    strArtistLogo,
    strFacebook,
    strTwitter,
    strLastFMChart,
  } = grupo;
  return (
    <div className="card border-light">
      <div className="card-header bg-primary text-light font-weight-bold">
        Información Artista
      </div>
      <div className="card-body">
        <img src={strArtistThumb} alt={strArtist} />
        <p className="card-text">
          Genero:{strGenre}
          <img src={strArtistLogo} alt={strArtist} />
        </p>

        <h2 className="card-text">Biografía:</h2>
        <p className="card-text">{strBiographyES}</p>
        <p className="card-text">
          {strFacebook !== '' ? (
            <a
              href={`https://${strFacebook}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className="fab fa-facebook"></i>
            </a>
          ) : null}
          {strTwitter !== '' ? (
            <a
              href={`https://${strTwitter}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className="fab fa-twitter"></i>
            </a>
          ) : null}
          {strLastFMChart !== null ? (
            <a
              href={`${strLastFMChart}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <i className="fab fa-lastfm"></i>
            </a>
          ) : null}
        </p>
      </div>
    </div>
  );
};

Grupo.propTypes = {
  grupo: PropTypes.object.isRequired,
};

export default Grupo;
