import React, { useState } from 'react';
import PropTypes from 'prop-types';

const Formulario = ({ setBuscarletra }) => {
  const [busqueda, setBusqueda] = useState({
    artista: '',
    cancion: '',
  });
  const { artista, cancion } = busqueda;
  const [error, setError] = useState(false);
  //funcion para leer el contenido de los inputs
  const handleBusqueda = (e) => {
    //e.preventDefault();
    setBusqueda({
      ...busqueda,
      [e.target.name]: e.target.value,
    });
  };
  const handleBuscaInfo = (e) => {
    e.preventDefault();
    if (artista.trim() === '' || cancion.trim() === '') {
      setError(true);
      return;
    }
    setError(false);
    setBuscarletra(busqueda);
  };

  return (
    <div className="bg-info mb-5">
      {error ? (
        <p className="alert alert-danger text-center p-2">
          Todos los campos son obligatorios
        </p>
      ) : null}
      <div className="container">
        <div className="row">
          <form
            className="col card text-white bg-transparent mb-5 pt-5 pb-2"
            onSubmit={handleBuscaInfo}
          >
            <fieldset>
              <legend className="text-center">
                Buscador Letras de Canciones
              </legend>
              <div className="row">
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Artista</label>
                    <input
                      type="text"
                      className="form-control"
                      name="artista"
                      placeholder="Nombre Artista"
                      onChange={handleBusqueda}
                    />
                  </div>
                </div>
                <div className="col-md-6">
                  <div className="form-group">
                    <label>Canción</label>
                    <input
                      type="text"
                      className="form-control"
                      name="cancion"
                      placeholder="Nombre Canción"
                      onChange={handleBusqueda}
                    />
                  </div>
                </div>
              </div>
              <button type="submit" className="btn btn-primary float-right">
                BUSCAR
              </button>
            </fieldset>
          </form>
        </div>
      </div>
    </div>
  );
};

Formulario.propTypes = {
  setBuscarletra: PropTypes.func.isRequired,
};

export default Formulario;
