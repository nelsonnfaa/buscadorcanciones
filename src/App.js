import React, { useState, useEffect } from 'react';
import Formulario from './components/Formulario/view';
import Axios from 'axios';
import Cancion from './components/Cancion/view';
import Grupo from './components/Grupo/view';

function App() {
  //state para recuperar la info del formulario
  const [buscarletra, setBuscarletra] = useState({});
  const [letra, setLetra] = useState('');
  const [grupo, setGrupo] = useState({});
  const [error, setError] = useState(false);
  useEffect(() => {
    if (Object.keys(buscarletra).length === 0) return;
    const consultarApiLetra = async () => {
      const { artista, cancion } = buscarletra;
      const url = `https://api.lyrics.ovh/v1/${artista}/${cancion}`;
      const urlG = `https://www.theaudiodb.com/api/v1/json/1/search.php?s=${artista}`;

      await Promise.all([Axios(url), Axios(urlG)])
        .then((values) => {
          const letraC = values[0];
          const informacion = values[1];
          setLetra(letraC.data.lyrics);
          setGrupo(informacion.data.artists[0]);
          setError(false);
        })
        .catch((err) => {
          setError(true);
          return;
        });
      // const [letraC, informacion] =
      // setLetra(letraC.data.lyrics);
      // setGrupo(informacion.data.artists[0]);
      // if (letra) {}
    };
    consultarApiLetra();
  }, [buscarletra]);
  return (
    <div className="App">
      <Formulario setBuscarletra={setBuscarletra} />
      <div className="container">
        <div className="row">
          {error ? (
            <div className="col-md-12 text-center">
              <h2>Cancion o Grupo no encontrados</h2>
            </div>
          ) : (
            <>
              <div className="col-md-6">
                <Grupo grupo={grupo} />
              </div>
              <div className="col-md-6">
                <Cancion letra={letra} />
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default App;
